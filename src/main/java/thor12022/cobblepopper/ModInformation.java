package thor12022.cobblepopper;

/*
 * Basic information your mod depends on.
 */

public class ModInformation
{

   public static final String NAME = "Cobble Popper";
   public static final String ID = "cobblepopper";
   public static final String CHANNEL = "CobblePopper";
   public static final String DEPEND = "";
   public static final String VERSION = "%VERSION%";
   public static final String CLIENTPROXY = "thor12022.cobblepopper.proxies.ClientProxy";
   public static final String COMMONPROXY = "thor12022.cobblepopper.proxies.CommonProxy";
}
