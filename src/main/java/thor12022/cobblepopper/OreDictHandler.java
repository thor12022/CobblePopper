package thor12022.cobblepopper;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class OreDictHandler
{
   // Oredict entries
   public static String blockObsidian = "blockObsidian";

   public static void registerOreDict()
   {
     if(!OreDictionary.doesOreNameExist(blockObsidian))
     {
        OreDictionary.registerOre(blockObsidian, new ItemStack(Blocks.obsidian, 1, 0));
     }
   }
}