package thor12022.cobblepopper;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import thor12022.cobblepopper.client.gui.CreativeTabBaseMod;
import thor12022.cobblepopper.config.ConfigManager;
import thor12022.cobblepopper.proxies.CommonProxy;
import thor12022.cobblepopper.util.TextHelper;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION, dependencies = ModInformation.DEPEND)
public class CobblePopper
{
   @Mod.Instance
   public static CobblePopper instance;
   
   @SidedProxy(clientSide = ModInformation.CLIENTPROXY, serverSide = ModInformation.COMMONPROXY)
   public static CommonProxy proxy;
   
   public static final Logger LOGGER = LogManager.getLogger(ModInformation.NAME);
   public static final Random RAND = new Random();
   
   public static ConfigManager config;
   public static CreativeTabs tabBaseMod = new CreativeTabBaseMod(ModInformation.ID + ".creativeTab");
   

   @Mod.EventHandler
   public void preInit(FMLPreInitializationEvent event)
   {
      LOGGER.info(TextHelper.localize("info." + ModInformation.ID + ".console.load.preInit"));

      config = new ConfigManager(event.getSuggestedConfigurationFile());

      proxy.preInit();
   }

   @Mod.EventHandler
   public void init(@SuppressWarnings("unused") FMLInitializationEvent event)
   {
      LOGGER.info(TextHelper.localize("info." + ModInformation.ID + ".console.load.init"));
      proxy.init();
   }
}
