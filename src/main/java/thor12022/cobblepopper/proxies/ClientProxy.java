package thor12022.cobblepopper.proxies;

import thor12022.cobblepopper.blocks.BlockRegistry;

public class ClientProxy extends CommonProxy
{
   @Override
   public void preInit()
   {
      super.preInit();
      BlockRegistry.registerModels();
   }   
}
