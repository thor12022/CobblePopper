package thor12022.cobblepopper.proxies;

import thor12022.cobblepopper.OreDictHandler;
import thor12022.cobblepopper.blocks.BlockRecipeRegistry;
import thor12022.cobblepopper.blocks.BlockRegistry;

public class CommonProxy
{
   public void preInit()
   {
      BlockRegistry.registerBlocks();
   }
   
   public void init()
   {
      OreDictHandler.registerOreDict();	  
      BlockRecipeRegistry.registerBlockRecipes();
   }
}
