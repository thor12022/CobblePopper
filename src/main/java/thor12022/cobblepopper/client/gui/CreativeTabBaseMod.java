package thor12022.cobblepopper.client.gui;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import thor12022.cobblepopper.ModInformation;
import thor12022.cobblepopper.blocks.BlockRegistry;

public class CreativeTabBaseMod extends CreativeTabs
{

   public CreativeTabBaseMod(String tabLabel)
   {
      super(tabLabel);
      setBackgroundImageName(ModInformation.ID + ".png"); // Automagically has
                                                          // tab_ applied to it.
                                                          // Make sure you
                                                          // change the texture
                                                          // name.
   }

   public boolean hasSearchBar()
   {
      return false;
   }

   // The tab icon is what you return here.
   @Override
   public ItemStack getIconItemStack()
   {
      return new ItemStack(BlockRegistry.blockCobblePopper);
   }

   @Override
   public Item getTabIconItem()
   {
      return new Item();
   }
}
