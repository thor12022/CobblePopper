package thor12022.cobblepopper.tiles;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.ITickable;
import thor12022.cobblepopper.CobblePopper;
import thor12022.cobblepopper.config.Config;
import thor12022.cobblepopper.config.Configurable;

@Configurable(sectionName="General")
public class TileEntityCobblePopper extends TileEntity implements ISidedInventory, ITickable
{
   private static final int MAX_STORAGE = 2;

   @Config(minFloat=0, maxFloat=2f)
   private static float xzLaunchVelocityMultiplier = 0.12f;
   
   @Config(minFloat=0, maxFloat=2f)
   private static float yLaunchVelocityMultiplier = 0.22f;

   @Config(minInt=2, comment="Delay between cobblestone production")
   private static int productionCycleTicks = 25;   
   
   private ItemStack cobbleStack = null;
   private int tickCount = 0;

   static
   {
      CobblePopper.config.register(TileEntityCobblePopper.class);
   }
   
   @Override
   public int getSizeInventory()
   {
      return 1;
   }

   /**
    * 
    * @returns true is there is a block with a solid bottom face above this
    *          block
    */
   private boolean isBlockCovered()
   {
      BlockPos blockPos = super.getPos().add(0, 1, 0);
      return worldObj.getBlockState(blockPos).getBlock().isSideSolid(worldObj, blockPos, EnumFacing.DOWN);
   }

   /**
    * 
    * @returns the number of lava-water pairs around the block
    */
   private int checkForLavaAndWater()
   {
      int lavaCount = 0, waterCount = 0;
      final EnumFacing[] checkDirections = { EnumFacing.NORTH, EnumFacing.SOUTH, EnumFacing.EAST, EnumFacing.WEST };
      for(EnumFacing dir : checkDirections)
      {
         BlockPos blockPos = getPos().add(dir.getFrontOffsetX(), 0, dir.getFrontOffsetZ());
         Block block = worldObj.getBlockState(blockPos).getBlock();
         lavaCount += (block == Blocks.lava) ? 1 : 0;
         waterCount += (block == Blocks.water) ? 1 : 0;
      }
      if(lavaCount == 2 && waterCount == 2)
      {
         return 2;
      }
      else if(lavaCount > 0 && waterCount > 0)
      {
         return 1;
      }
      else
      {
         return 0;
      }
   }

   /**
    * Ejects a stack of a single cobblestone n the air
    * 
    * @pre this.cobbleStack must be non-null and valid
    * @post this.cobbleStack stack size is decremented, if stack size 0,
    *       this.cobbleStack set to null
    */
   private void launchACobble()
   {
      if(cobbleStack == null)
      {
         return;
      }

      ItemStack launchStack = cobbleStack.splitStack(1);
      if(cobbleStack.stackSize == 0)
      {
         cobbleStack = null;
      }
      EntityItem entityItem = new EntityItem(worldObj, getPos().getX() + 0.5f, getPos().getY() + 1.25, getPos().getZ() + 0.5f, launchStack);
      entityItem.setPickupDelay(5);
      entityItem.motionX = CobblePopper.RAND.nextGaussian() * xzLaunchVelocityMultiplier;
      entityItem.motionY = Math.abs(CobblePopper.RAND.nextGaussian()) * yLaunchVelocityMultiplier;
      entityItem.motionZ = CobblePopper.RAND.nextGaussian() * xzLaunchVelocityMultiplier;
      worldObj.spawnEntityInWorld(entityItem);
   }

   @Override
   public void update()
   {
      if(worldObj.isRemote)
      {
         return;
      }

      ++tickCount;

      if(tickCount == productionCycleTicks)
      {
         if(!worldObj.isBlockPowered(getPos()))
         {
            if(cobbleStack != null && !isBlockCovered())
            {
               launchACobble();
            }

            if(cobbleStack == null && checkForLavaAndWater() > 0)
            {
               cobbleStack = new ItemStack(Item.getItemFromBlock(Blocks.cobblestone), 1);
            }
         }
      }
      else if(tickCount == productionCycleTicks / 2)
      {
         if(!worldObj.isBlockPowered(getPos()))
         {
            if(cobbleStack != null && !isBlockCovered())
            {
               launchACobble();
            }

            if(checkForLavaAndWater() > 1)
            {
               if(cobbleStack == null)
               {
                  cobbleStack = new ItemStack(Item.getItemFromBlock(Blocks.cobblestone), 1);
               }
               else if(cobbleStack.stackSize < MAX_STORAGE)
               {
                  cobbleStack.stackSize++;
               }
            }
         }
      }

      if(tickCount == productionCycleTicks + 1)
      {
         tickCount = 0;
      }
   }

   @Override
   public ItemStack getStackInSlot(int slot)
   {
      if(slot == 0)
      {
         return cobbleStack;
      }
      return null;
   }

   @Override
   public ItemStack decrStackSize(int slot, int number)
   {
      if(cobbleStack != null && slot == 0 && number > 0)
      {
         ItemStack stack = cobbleStack.splitStack(1);
         if(cobbleStack.stackSize == 0)
         {
            cobbleStack = null;
         }
         return stack;
      }
      return null;
   }

   @Override
   public void setInventorySlotContents(int p_70299_1_, ItemStack p_70299_2_)
   {
   }

   @Override
   public String getName()
   {
      return null;
   }

   @Override
   public boolean hasCustomName()
   {
      return false;
   }

   @Override
   public int getInventoryStackLimit()
   {
      return 0;
   }

   @Override
   public boolean isUseableByPlayer(EntityPlayer p_70300_1_)
   {
      return false;
   }

   @Override
   public void openInventory(EntityPlayer player)
   {
   }

   @Override
   public void closeInventory(EntityPlayer player)
   {
   }

   @Override
   public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_)
   {
      return false;
   }

   @Override
   public int[] getSlotsForFace(EnumFacing side)
   {
      int[] result = new int[1];
      result[0] = 0;
      return result;
   }

   @Override
   public boolean canInsertItem(int p_102007_1_, ItemStack p_102007_2, EnumFacing side)
   {
      return false;
   }

   @Override
   public boolean canExtractItem(int slot, ItemStack itemStack, EnumFacing side)
   {
      if(slot == 0)
      {
         return itemStack == cobbleStack;
      }
      return false;
   }

   @Override
   public ItemStack removeStackFromSlot(int index)
   {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public int getField(int id)
   {
      // TODO Auto-generated method stub
      return 0;
   }

   @Override
   public void setField(int id, int value)
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public int getFieldCount()
   {
      // TODO Auto-generated method stub
      return 0;
   }

   @Override
   public void clear()
   {
      // TODO Auto-generated method stub
      
   }

   @Override
   public IChatComponent getDisplayName()
   {
      // TODO Auto-generated method stub
      return null;
   }

}
