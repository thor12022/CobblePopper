package thor12022.cobblepopper.util;

import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import thor12022.cobblepopper.CobblePopper;
import thor12022.cobblepopper.ModInformation;

public class EventHandler
{
   @SubscribeEvent
   public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs)
   {
      if(eventArgs.modID.equals(ModInformation.ID))
      {
         CobblePopper.config.syncConfig();
         CobblePopper.LOGGER.info(TextHelper.localize("info." + ModInformation.ID + ".console.config.refresh"));
      }
   }
}
