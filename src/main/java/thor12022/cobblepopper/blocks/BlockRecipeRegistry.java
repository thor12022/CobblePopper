package thor12022.cobblepopper.blocks;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class BlockRecipeRegistry
{

   // Self explanatory. Continue these how you wish. EG:
   // registerPulverizerRecipes
   private static void registerShapedRecipes()
   {
      GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(BlockRegistry.blockCobblePopper), new Object[]{"coc", "opo", "coc", 'c', "cobblestone", 'o', "blockObsidian", 'p', new ItemStack(Blocks.piston)}));
   }

   private static void registerShaplessRecipes()
   {

   }

   public static void registerBlockRecipes()
   {
      registerShapedRecipes();
      registerShaplessRecipes();
   }
}
