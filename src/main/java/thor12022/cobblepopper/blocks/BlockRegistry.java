package thor12022.cobblepopper.blocks;

import net.minecraft.block.Block;
import net.minecraftforge.fml.common.registry.GameRegistry;
import thor12022.cobblepopper.ModInformation;
import thor12022.cobblepopper.tiles.TileEntityCobblePopper;

public class BlockRegistry
{

   public static Block blockCobblePopper = new BlockCobblePopper();

   public static void registerBlocks()
   {
      GameRegistry.registerBlock(blockCobblePopper, BlockCobblePopper.NAME);
      GameRegistry.registerTileEntity(TileEntityCobblePopper.class, ModInformation.ID + ":" + BlockCobblePopper.NAME);
   }
   
   public static void registerModels()
   {
      ((BlockCobblePopper) blockCobblePopper).initModel();
   }
}
