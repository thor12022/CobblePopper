package thor12022.cobblepopper.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import thor12022.cobblepopper.CobblePopper;
import thor12022.cobblepopper.ModInformation;
import thor12022.cobblepopper.tiles.TileEntityCobblePopper;
import thor12022.cobblepopper.util.KeyboardHelper;
import thor12022.cobblepopper.util.TextHelper;

public class BlockCobblePopper extends BlockContainer
{
   public static final String NAME = "cobblePopper";

   public BlockCobblePopper()
   {
      super(Material.rock);
      setUnlocalizedName(ModInformation.ID + "." + NAME);
      setCreativeTab(CobblePopper.tabBaseMod);
      setStepSound(Block.soundTypeStone);
      setHardness(3.5f);

      MinecraftForge.EVENT_BUS.register(this);
   }

   @Override
   public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_)
   {
      return new TileEntityCobblePopper();
   }
   
   @Override
   public int getRenderType()
   {
     return 3;
   }
   
   public void initModel()
   {
       ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(ModInformation.ID + ":" + NAME));
   }
   
   @SubscribeEvent
   public void onItemTooltip(ItemTooltipEvent event)
   {
      if(Block.getBlockFromItem(event.itemStack.getItem()) instanceof BlockCobblePopper)
      {
         if(KeyboardHelper.isShiftDown())
         {
            event.toolTip.add(TextHelper.localize(getUnlocalizedName() + ".shiftInfo1"));
            event.toolTip.add(TextHelper.localize(getUnlocalizedName() + ".shiftInfo2"));
         }
         else
         {
            event.toolTip.add(TextHelper.localize(getUnlocalizedName() + ".info"));
         }
      }
   }
}
